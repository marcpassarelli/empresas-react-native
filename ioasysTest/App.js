
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation'
import { SplashScreen } from './src/screens/splash/SplashScreen'
import { LoginScreen } from './src/screens/login/LoginScreen'
import { HomeScreen } from './src/screens/home/HomeScreen'
import { DetalhesEmpresaScreen } from './src/screens/detalhesEmpresa/DetalhesEmpresaScreen'

const AppStack = createStackNavigator({
  Splash: { screen: SplashScreen },
  Login: { screen: LoginScreen },
  Home: { screen: HomeScreen },
  DetalhesEmpresa: { screen: DetalhesEmpresaScreen }
}
)

const App = createAppContainer(AppStack);

export default App;
