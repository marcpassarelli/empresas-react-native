const colors = {
  primary: '#000000',
  secondary: '#FFFFFF',
  details: '#8b1542'
}

export default colors
