import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import FadeInOutView from './FadeInOutView'
import { Image, View, StyleSheet } from 'react-native';
import images from 'res/images'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export class SplashScreen extends Component{

  static navigationOptions = {
     header: null,
  };

  goToLogin=()=>{
    //Navega até a tela login
    this.props.navigation.navigate('Login')
  }

  componentDidMount(){
    //Espera os 3 segundos da animação e então chama a função para navegar até a tela de login
    setTimeout (() => {
      this.goToLogin()
    }, 3000);
  }


  render() {
    return (
      <FadeInOutView style={styles.container}>
        <Image
          style={styles.logo}
         source={images.logo}
        />
      </FadeInOutView>
    )
    }
  }

const styles = StyleSheet.create({
  container:{
    justifyContent: 'center',
    alignItems: 'center',
    flex:1
  },
  logo:{
    width:wp('80%'),
    height: hp('20%'),
    resizeMode: 'contain'
  }
})
