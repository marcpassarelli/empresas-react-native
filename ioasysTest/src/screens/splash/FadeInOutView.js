import React, { Component } from 'react';
import {
  Animated,
} from 'react-native';

//Classe responsavel por fazer o animação/efeito de aparecer e sumir da imagem na Splash Screen
export default class FadeInOutView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fadeAnim: new Animated.Value(0), // Valor inicial para opacidade: 0, completamente transparente
    };
  }
  componentDidMount() {

    Animated.sequence([
      Animated.timing(   // Anima conforme o tempo de duração
        this.state.fadeAnim,
        {
          toValue: 1,   // Anima para opacidade: 1, ou completamente opaco para exibir a imagem
          duration:1500,
        }
      ),
      Animated.timing(
        this.state.fadeAnim,
        {
          toValue: 0,  // Anima para opacidade: 1, ou completamente transparente para ocultar a imagem
          duration: 1500,
        }
      )]).start();
  }

  render() {
    return (
      <Animated.View
        style={{
          ...this.props.style,
          opacity: this.state.fadeAnim,
        }}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}
