
import React, { Component } from 'react';
import { Image, View, Text, TouchableOpacity,StyleSheet } from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

/*Responsável por renderizar cada célula da lista de empresas*/
export default class ListaDeEmpresasRenderItem extends Component {

  render() {
    return (
      <TouchableOpacity
        style={styles.containerListItem}
        onPress = {this.props.navigation}>
        <View>
          <Text
            style={styles.textNome}>{this.props.nomeEmpresa}</Text>
          <Text
            style={styles.textCidade}>{this.props.cidade}, {this.props.pais}</Text>
        </View>
      </TouchableOpacity>
    )
  }

}



const styles = StyleSheet.create({
  containerListItem:{
    flexDirection:'row',
    height:hp('8%'),
    alignItems:'center',
    marginLeft: wp('5%'),
    flex:1,
    marginBottom: 5,
    marginTop:3,
  },
  textNome:{
    fontFamily: 'Futura-Medium',
    fontSize:wp('5%')
  },
  textCidade:{
    fontFamily: 'Futura-Medium',
    fontSize:wp('3%'),
    marginLeft: 2
  }
})
