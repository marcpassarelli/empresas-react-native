import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { Image, View, StyleSheet,FlatList,ActivityIndicator,Text,TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import colors from 'res/colors'
import ListaDeEmpresasRenderItem from './ListaDeEmpresasRenderItem'
import ModalFiltro from './ModalFiltro'
import LoadingModal from 'common/components/LoadingModal';

const tiposEmpresa = []

export class HomeScreen extends Component{

  static navigationOptions = ({navigation}) => ({
    headerMode:'float',
    title: 'Lista de Empresas',
    headerTitleStyle: styles.headerText,
    headerStyle: styles.header,
    headerLeft: (<View></View>),
    headerRight:
    (
      <View style={{marginRight: 8}}>
        <TouchableOpacity onPress={navigation.getParam('filtrar')}>
          <Text style={{color: colors.secondary,fontSize: hp('2%'),fontFamily:'Futura-Medium' }}>Filtrar</Text>
        </TouchableOpacity>
      </View>
    )
  })


  constructor(props){
    super(props);

    this.state = {
      listaDeEmpresas:'', // state para salvar as informações obtidas através da chamada na API
      listaDeEmpresasFiltradas:'', //state que será modificado quando houver filtros
      loading:false, // state para ativar o ActivityIndicator até as informações serem carregadas
      modalVisible: false,
      buscaEmpresa:'' ,// state para guardar a string que será utilizada para fazer a busca com o filtro
      loadingModal:false
    }

  }

  //função que deixa o modal do filtro visível
  _filtrar=()=>{
    this.setState({
      modalVisible: true
    });
  }

  //
  showModal(){
    this.setState({
      modalVisible:!this.state.modalVisible
    });
  }

  /*cada vez que o texto to TextInput é modificado essa função será chamada
  para atualizar o state que controla o valor do TextInput*/
  updateBuscaEmpresa = (text) => {
    this.setState({buscaEmpresa: text})
  }

  /*cada vez que um novo tipo de empresa for escolhido no Picker,
  essa função será chamada para atualizar o state que controla o valor selecionado*/
  updateTipoEmpEsc = (value) => {
     this.setState({ tipoEmpEsc: value })
  }


  componentDidMount(){
    //passar a função _filtrar para o Header
    this.props.navigation.setParams({ filtrar: this._filtrar })

    //ativa o ActivityIndicator enquanto a informação é buscada
    this.setState({
      loading:true
    });

    const {navigation} = this.props
    /*Faz a chamada na API com access-token, client e uid que foram
      passados da tela de login*/
    fetch('http://empresas.ioasys.com.br/api/v1/enterprises',{
      method:'GET',
      headers:{
        "Content-Type": "application/json",
        "access-token": navigation.getParam('accessToken',''),
        "client": navigation.getParam('client',''),
        "uid": navigation.getParam('uid',''),
      }
    })
    .then(res => res.json())
    .then(resJson => {
      this.setState({
        listaDeEmpresas: resJson.enterprises,
        listaDeEmpresasFiltradas: resJson.enterprises,
        loading:false
      },function(){
        //pegar os tipos de empresa para alimentar o Picker para o filtro
        this.state.listaDeEmpresas.map((item,index)=>{
          tiposEmpresa.push(item.enterprise_type)
        })
      });
    })
  }

//os separadores das células da FlatList
  renderSeparator = () => {
    return (
      <View
        style={{height: 3, backgroundColor: colors.details,
          marginHorizontal: 10, marginBottom:hp('0.77%')}}
      />
    );
  };

  limparFiltro(){
    this.setState({
      loading:true
    },function(){
      //atribui a lista inicial para listar todos os items novamente
      this.setState({
        listaDeEmpresasFiltradas: this.state.listaDeEmpresas,
        modalVisible: false
      },function(){
        this.setState({
          loading:false
        });
      });
    });
  }

  aplicarFiltro(){
      const {navigation} = this.props
      //mostra o modal de loading
    this.setState({
      loading:true,
      loadingModal:true
    });

    /*Faz a chamada na API com access-token, client e uid que foram
      passados da tela de login e passa o tipo de empresa e uma string com o nome da empresa que quer buscar*/
    fetch('http://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types='+this.state.tipoEmpEsc+'&name='+this.state.buscaEmpresa,{
      method:'GET',
      headers:{
        "Content-Type": "application/json",
        "access-token": navigation.getParam('accessToken',''),
        "client": navigation.getParam('client',''),
        "uid": navigation.getParam('uid',''),
      }
    })
    .then(res => res.json())
    .then(resJson => {
      /*Atualiza a lista da Flatlist, fecha o modal do filtro e o modal de loading*/
      this.setState({
        listaDeEmpresasFiltradas: resJson.enterprises,
        modalVisible: false,
        loading:false,
        loadingModal:false
      });
    })
  }


  render() {

    const content = this.state.loading ?
    /*enquanto loading for true mostrará o ActivityIndicator para o usuário
      indicando que há informações sendo carregadas*/
    <View style={styles.containerIndicator}>
      <ActivityIndicator size="large" color={colors.details} />
    </View> :

    /*Verifica se a lista está vazia após o filtro, se não estiver vazia
    renderiza a FlatList para mostrar a lista de empresas e caso não tenha encontrado nada
    mostrará um texto ao usuário informando que não há empresas com os parametros filtrados*/
    <View>
      {Object.keys(this.state.listaDeEmpresasFiltradas).length>0?
        <FlatList
          ItemSeparatorComponent={this.renderSeparator}
          data= {this.state.listaDeEmpresasFiltradas}
          renderItem= {({item}) =>
          <ListaDeEmpresasRenderItem
            nomeEmpresa={item.enterprise_name}
            cidade={item.city}
            pais={item.country}
            descricao={item.description}
            navigation={()=>{
              const {navigation} = this.props
              /*Navega para Home com as
              informações necessárias para autorizar as próximas requisições*/
              navigation.navigate('DetalhesEmpresa',{
                nomeEmpresa:item.enterprise_name,
                id: item.id,
                accessToken: navigation.getParam('accessToken',''),
                client: navigation.getParam('client',''),
                uid: navigation.getParam('uid',''),
              })
            }}>
          </ListaDeEmpresasRenderItem>}
          keyExtractor={item => item.id.toString()}/>

      :

        <View style={{justifyContent: 'center',alignContent: 'center'}}>
          <Text style={{alignSelf: 'center',
            fontFamily: 'Futura-Medium',
            marginTop: 10}}>Não há nenhuma empresa com os filtros selecionados.</Text>
        </View>}

    </View>

    return (
      <View style={{flex:1}}>
        <LoadingModal
          loadingModal={this.state.loadingModal}/>
        <ModalFiltro
          buscaEmpresa={this.state.buscaEmpresa}
          updateBuscaEmpresa={this.updateBuscaEmpresa}
          tiposEmpresa={tiposEmpresa}
          tipoEmpEsc={this.state.tipoEmpEsc}
          updateTipoEmpEsc={this.updateTipoEmpEsc}
          modalVisible={this.state.modalVisible}
          showModal={()=>{this.showModal()}}
          onPressBtn={()=>{this.onPressBtn()}}
          aplicarFiltro={()=>{this.aplicarFiltro()}}
          limparFiltro={()=>{this.limparFiltro()}}/>
        {content}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  containerIndicator:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  header:{
    backgroundColor: colors.details
  },
  headerText:{
    fontFamily: 'FuturaPT-Bold',
    alignSelf: 'center',
    fontSize: wp('7%'),
    marginBottom: 5,
    color: colors.secondary
  }
})
