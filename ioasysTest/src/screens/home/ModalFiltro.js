import React, { Component } from 'react';
import colors from 'res/colors'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { StyleSheet, View, Modal, FlatList, TouchableOpacity, TextInput, Text,Picker} from 'react-native';

/*Modal que será mostrado ao usuário caso ele queira utilizar o filtro*/
export default class ModalFiltro extends Component {

render(){
    return (
      <Modal transparent={true}
      animationType={'none'}
      visible={this.props.modalVisible}
      onRequestClose={() => {this.props.showModal()}}>
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <Text style={styles.textTitle}>Filtre pelo nome e tipo de empresa:</Text>
            <View style={{flexDirection: 'column', justifyContent: 'center',alignContent: 'center'}}>
              <View>
              <TextInput
                underlineColorAndroid='transparent'
                autoCapitalize='none'
                style={styles.containerTextInput}
                placeholder={'EMPRESA'}
                value={this.props.buscaEmpresa}
                onChangeText={this.props.updateBuscaEmpresa}
                  >
              </TextInput>
              </View>
              <View>
              <Picker
                style={{marginLeft: wp('13.5%'),height: hp('8%'),justifyContent: 'center',alignContent: 'center',width: wp('70%'),}}
                selectedValue={this.props.tipoEmpEsc}
                onValueChange={this.props.updateTipoEmpEsc}>
                {this.props.tiposEmpresa.map((item, index)=>{
                  return (<Picker.Item label={item.id+" - "+item.enterprise_type_name}
                    value={item.id} key={index}color={colors.details} />)
                })}
              </Picker>
              </View>
            </View>
            <View style={{flexDirection: 'column', alignItems: 'center',alignSelf: 'center',position: 'absolute',bottom: 10}}>
              <View style={{}}>
                <TouchableOpacity
                  onPress={()=>{this.props.aplicarFiltro()}}>
                  <Text style={styles.text2}>Aplicar Filtro</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={()=>{this.props.limparFiltro()}}>
                  <Text style={styles.text2}>Limpar Filtro</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={()=>{this.props.showModal()}}>
                <Text style={styles.text2}>Fechar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
    }
  }

  const styles = StyleSheet.create({
    textTitle:{alignSelf: 'center',
      fontSize:wp('4%'),
      color: colors.primary,
      marginVertical: 10,
      fontFamily: 'Futura-Medium'
    },
    text2:{
      fontSize:wp('5%'),
      marginVertical: hp('1%'),
      color: colors.details,
      fontFamily: 'Futura-Medium'
    },
    containerTextInput:{
      paddingLeft: 15,
      backgroundColor: '#d1d1d1',
      borderColor: colors.details,
      borderWidth: 1,
      alignSelf: 'center',
      color: colors.primary,
      fontSize: wp('4%'),
      height: hp('7%'),
      width: wp('63.30%'),
      fontFamily: 'Futura-Medium'
    },
    modalBackground: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
      backgroundColor: 'rgba(0, 0, 0,0.4)'
    },
    activityIndicatorWrapper: {
      backgroundColor: colors.secondary,
      width: wp('95%'),
      height: hp('50%'),
      borderRadius: 10,
      flexDirection: 'column'
    }
  })
