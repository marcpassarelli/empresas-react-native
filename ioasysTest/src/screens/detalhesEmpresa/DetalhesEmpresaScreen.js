import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { Image, View, StyleSheet,ActivityIndicator,Text,TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import colors from 'res/colors'

export class DetalhesEmpresaScreen extends Component{

  static navigationOptions = ({navigation}) => ({
    headerMode:'float',
    title: 'Detalhes da Empresa',
    headerTitleStyle: styles.headerText,
    headerStyle: styles.header
  })


  constructor(props){
    super(props);

    this.state = {
      infoEmpresa:'',
      loading:false
    }

  }

  componentDidMount(){
    this.setState({
      loading:true,

    });
    const {navigation} = this.props
    //busca pela empresa conforme a ID passada pela página anterior
    fetch('http://empresas.ioasys.com.br/api/v1/enterprises/'+navigation.getParam('id',''),{
      method:'GET',
      headers:{
        "Content-Type": "application/json",
        "access-token": navigation.getParam('accessToken',''),
        "client": navigation.getParam('client',''),
        "uid": navigation.getParam('uid',''),
      }
    })
    .then(res => res.json())
    .then(resJson => {
      this.setState({
        infoEmpresa: resJson.enterprise,
        loading:false
      });
    })
  }


  render() {

    const content = this.state.loading ?

    <View style={styles.containerIndicator}>
      <ActivityIndicator size="large" color={colors.details} />

    </View> :

    <View>
      <Text style={[styles.textTitle,{marginTop:10}]}>Nome da empresa:</Text>
      <Text style={styles.textContent}>{this.state.infoEmpresa.enterprise_name}</Text>
      <Text style={styles.textTitle}>Descrição:</Text>
      <Text style={styles.textContent}>{this.state.infoEmpresa.description}</Text>
      <Text style={styles.textTitle}>Cidade em que está localizada:</Text>
      <Text style={styles.textContent}>{this.state.infoEmpresa.city}</Text>
      <Text style={styles.textTitle}>País:</Text>
      <Text style={styles.textContent}>{this.state.infoEmpresa.country}</Text>
    </View>

    return (
      <View style={{flex:1}}>
        {content}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  containerIndicator:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  header:{
    backgroundColor: colors.details,

  },
  headerText:{
    fontFamily: 'FuturaPT-Bold',
    alignSelf: 'center',
    fontSize: wp('7%'),
    marginBottom: 5,
    color: colors.secondary
  },
  textTitle:{
    fontFamily: 'FuturaPT-Bold',
    marginLeft:wp('3%'),
    fontSize:wp('5%')
  },
  textContent:{
    fontFamily: 'Futura-Medium',
    marginLeft:wp('5%'),
    marginBottom:wp('3%'),
    color:colors.details,
    fontSize:wp('4.5%')
  }
})
