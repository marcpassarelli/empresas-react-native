import React, { Component } from 'react';
import { Image, View, StyleSheet,Alert } from 'react-native';
import NewTextInput from 'common/components/NewTextInput'
import NewButton from 'common/components/NewButton'
import LoadingModal from 'common/components/LoadingModal';
import { Header } from 'react-navigation-stack';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import images from 'res/images'

export class LoginScreen extends Component{

  static navigationOptions = {
    header: null,
  };

  constructor(props){
    super(props);
    this.state = {
      textEmail:'',
      textSenha:'',
      loading:false
    }

  }
  //chamado a cada vez que o texto do TextInput muda para atualizar o state que guarda o valor do TextInput
  updateTextEmail = (text) => {
    this.setState({textEmail: text})
  }
  updateTextSenha = (text) => {
    this.setState({textSenha: text})
  }

  loginToHome=()=>{
    this.setState({
      loading:true
    });
    const {navigate} = this.props.navigation
    //verifica se os campos de e-mail e senha estão vazios
    if(this.state.textEmail&&this.state.textSenha){
      //Usando Fetch API para consumir a API
      fetch('https://empresas.ioasys.com.br/api/v1/users/auth/sign_in', {
        method: 'POST',
        headers:
        {
          'Content-Type': 'application/json' },
          body: JSON.stringify({email: this.state.textEmail, password: this.state.textSenha}),
      })
      .then((response)=>{
        /*Se tiver sucesso então vou navegar para Home com as
        informações necessárias para autorizar as próximas requisições*/
        if(response.status==200){
          this.setState({
            loading:false
          });
          navigate('Home',{
            accessToken:response.headers.get('access-token'),
            client:response.headers.get('client'),
            uid:response.headers.get('uid')
          })
        }
        //Caso login ou senha esteja incorreto mostrará um Alert para o usuário informando
        else if(response.status==401){
          this.setState({
            loading:false
          });
          Alert.alert(
            'Login ou Senha incorretos',
            'Verifique se as informações inseridas estão corretas',
            [
              {text: 'OK', onPress: () => {

              }},
            ],
            { cancelable: false }
          )
        }
      })
      .catch((error) => {
        console.error("error: "+error);
      });
      }else{
        //Informa o usuário que algum dos campos está em branco
        Alert.alert(
          'Campos Vazios',
          'Preencha todos os campos para proceder com o login',
          [
            {text: 'OK', onPress: () => {

            }},
          ],
          { cancelable: false }
        )
      }

    }
    render() {
      return (
        <View style={styles.container}>
          <LoadingModal
            loadingModal={this.state.loading}/>
          <Image
            source={images.logo}
            style={styles.logo}/>
          <NewTextInput
            style={{marginBottom: hp('2.3%')}}
            sourceIcon={images.iconEmail}
            placeholder={'E-MAIL'}
            onChangeText={this.updateTextEmail}/>
          <NewTextInput
            style={{marginBottom: hp('3%')}}
            sourceIcon={images.iconSenha}
            placeholder={'SENHA'}
            secureTextEntry={true}
            onChangeText={this.updateTextSenha}/>
          <NewButton
            style={{marginBottom: hp('3%')}}
            onPress = {()=>{this.loginToHome()}}
            text={"LOGIN"} >
          </NewButton>
          </View>
      )
    }
  }

  const styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    logo:{
      marginVertical: hp('8%'),
        width:wp('80%'),
        height: hp('20%'),
      alignSelf: 'center',
      resizeMode: 'contain'
    }
    })
