
import { View, TextInput,Image,StyleSheet } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import React, { Component } from 'react';
import colors from 'res/colors'

//Component para um textinput personalizado para ser reutilizado em todo o app

export default class NewTextInput extends Component {

  render(){
    return (
      <View style={[this.props.style,{justifyContent: 'center',
        alignItems: 'center',flexDirection: 'row'}]}>
        <View style={styles.containerIcon}>
          <Image
            source={this.props.sourceIcon}
            style={styles.iconImg}
            />
        </View>
        <TextInput
          {...this.props}
          underlineColorAndroid='transparent'
          autoCapitalize='none'
          style={styles.containerTextInput}
            >
          </TextInput>
        </View>

      )
    }

  }

const styles = StyleSheet.create({
  containerTextInput:{
    alignItems: 'center',
    backgroundColor: '#d1d1d1',
    borderColor: colors.details,
    borderWidth: 1,
    color: colors.primary,
    fontSize: wp('4%'),
    height: hp('7%'),
    paddingLeft: 15,
    width: wp('63.30%'),
    fontFamily: 'Futura-Medium'
  },
  containerIcon:{
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: colors.details,
    height: hp('7%'),
    justifyContent: 'center',
    width: wp('14%'),
  },
  iconImg:{
    height: wp('8%'),
    width: wp('8%'),
    resizeMode: 'contain'
  }
})
