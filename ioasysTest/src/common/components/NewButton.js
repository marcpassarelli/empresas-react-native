import { View, TouchableHighlight, Text, StyleSheet } from 'react-native';
import React, { Component } from 'react';
import colors from 'res/colors'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

//Component para um botão personalizado para ser reutilizado em todo o app

export default class NewButton extends Component {
  render(){
    return (
      <View style={this.props.style}>
        <TouchableHighlight
          style={[styles.buttons,this.props.styleButton]}
          onPress = { () => {this.props.onPress()} }>
          <Text style={[styles.textButtons,this.props.styleText]}>{this.props.text}</Text>
        </TouchableHighlight>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  buttons:{
    height: hp('5.94%'),
    width: wp('75.28%'),
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: colors.details,
    borderRadius: 3
  },
  textButtons:{
    backgroundColor:'transparent',
    alignSelf:'center',
    color:colors.secondary,
    fontSize: wp('5%'),
    fontFamily: 'Futura-Medium'
  },
})
