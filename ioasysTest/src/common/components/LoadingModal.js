import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  ActivityIndicator,
  Text
} from 'react-native';
import colors from 'res/colors'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


export default class LoadingModal extends Component{
  render(){
    return(
      <Modal
        transparent={true}
        animationType={'none'}
        visible={this.props.loadingModal}
        onRequestClose={() => {}}>
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <Text style={{marginHorizontal: 5,
                fontFamily: 'Futura-Medium',
                textAlign: 'center',marginBottom: 5}}>Aguarde enquanto carregamos as informações necessárias</Text>
            <ActivityIndicator size="large" color={colors.details} />
          </View>
        </View>
      </Modal>
    )
  }

}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: 'rgba(0, 0, 0,0.4)'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: hp('35%'),
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
});
//
// export default Loader;
