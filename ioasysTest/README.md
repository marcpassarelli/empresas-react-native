# Desafio React Native - ioasys - Marc Victor Passarelli de Carvalho

### Instruções para executar o projeto ###

* Clonar a pasta do projeto
* Executar o comando `npm install` de dentro da pasta do projeto para os módulos serem instalados
* Para iOS vá até a pasta `ios` e rode o comando `pod install` 
* Então executar o comando `react-native run-android` ou `react-native run-ios`


### Justificativa para o uso das bibliotecas adicionadas ###

* react-navigation, react-native-gesture-handler, react-native-reanimated, react-native-screens e react-navigation-stack:
			Todas essas bibliotecas acima são bibliotecas necessárias para as navegações entre telas funcionarem corretamente entre outras coisas.

* react-native-responsive-screen:
			Essa biblioteca torna o trabalho de fazer o layout responsivo mais fácil. Usando ela como unidade de tamanho eu consigo aplicar para todas propriedades de Style no React Native, o que eu não conseguiria fazer com a % normal utilizada em web dev para propriedades como `margin`, `border-width`, `border-radius` e muitas outras propriedades.


### Notas ###

* Passei um tempo programando com React Native e Firebase e agora que estou retornando para o mercado de trabalho estou procurando me atualizar e estudar novos conceitos que não estava acostumado, como consumir APIs que aprendi agora e Redux que ainda estou aprendendo e resolvi não utilizar aqui neste projeto por achar que não iria conseguir aprender rápido o suficiente para utilizar nesse projeto.

* Os arquivos `package.json` dentro das pastas `res` e `common` tornam essas pastas em módulos para que o `node` possa achá-los, dessa forma ao importar os arquivos de dentro dessas pastas quando preciso usá-los eu posso usar o caminho absoluto sem a necessidade de utilizar `..` para voltar pastas

* Talvez eu tenha entendido errado (e se for o caso peço desculpas já adiantadas), mas achei redundante o uso dos Endpoints de detalhamento de empresas e o de filtro, pois com as informações obtidas através da listagem de empresas eu poderia apenas manipular esses dados para conseguir o detalhamento e fazer os filtros, ao invés de fazer novas chamadas à API.
